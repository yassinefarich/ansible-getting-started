# Ansible getting started

Note : this project is under construction
## Project 
This project contains an example of Ansible.

## Vagrant Machines
To start machines you need to run `vagrant up` this will create three achine as below :

	- Meknes : Web server machine contains Nginx
	- Fes : Database machine
	- Rabat : CI machine (Jenkins)
	- Ansible : ansible machine

## How to run Ansible main playbook
To run main playbook you need to log in Ansible machine using `vagrant ssh ansible` and run `/ansible/RunPlaybook.sh` 